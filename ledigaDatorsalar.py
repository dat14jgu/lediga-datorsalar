import urllib.request
import datetime
import sys
import csv
import argparse

def timeToIndex(time):
    if time == 9:
        return 0
    elif time == 11:
        return 1
    elif time == 14:
        return 2
    elif time == 16:
        return 3

indexToTime = ["08-10", "10-12", "13-15", "15-17"]

windowsRooms = ["E:Elg", "E:Eka", "E:Rav", "E:Satu", "E:Uran", "E:Nept", "E:Plu"]

translate = {}

orderDict = {
    "E:Hack"    : 1,
    "E:Pant"    : 2,
    "E:Elg"     : 3,
    "E:Eka"     : 4,
    "E:Rav"     : 5,
    "E:Val"     : 6,
    "E:Falk"    : 7,
    "E:Varg"    : 8,
    "E:Lo"      : 9,

    "E:Venu"    : 10,
    "E:Mars"    : 11,
    "E:Jupi"    : 12,
    "E:Satu"    : 13,
    "E:Uran"    : 14,
    "E:Nept"    : 15,
    "E:Plu"     : 16,

    "E:Alfa"    : 17,
    "E:Beta"    : 18,
    "E:Gamma"   : 19
}

def order(room):
    return orderDict[room]

def niceify(room):
    name = translate[room][2:]
    if room in windowsRooms:
        return name + "*"
    else:
        return name

def updateSchedule(url, mydate):
    response = urllib.request.urlopen(url)
    data = response.read()
    text = data.decode('utf-8')
    text = text.split('\n')

    rooms = []
    rawRooms = text[2][1:].split(',')
    for i in range(1, len(rawRooms)-1, 2):
        name = rawRooms[i].replace(' ', '').replace('"', '')
        rooms.append(name)
        translate[name] = rawRooms[i-1]

    # Headers can be found in the fourth line, should use them. 
    csv_reader = csv.reader(text, delimiter=',')

    roomDict = {}
    for room in rooms:
        roomDict[room] = [True, True, True, True]

    for row in csv_reader:
        if len(row) > 2 and row[0] == mydate.strftime("20%y-%m-%d"):
            for room in row[10].split(','):
                start = int(row[1][:2])
                end = int(row[3][:2])
                for time in [9, 11, 14, 16]:
                    if time > start and time < end: # Legal booking
                        index = timeToIndex(time)
                        roomDict[room][index] = False

    rooms = sorted(rooms, key=order)
    return (rooms, roomDict)

def outputTerminal(mydate, rooms, roomDict):
    FREE = '\033[92m'
    TAKEN = '\033[91m'
    ENDCOL = '\033[0m'
    print("Status för datorsalar", mydate.strftime("20%y-%m-%d"), "\n")
    for room in rooms:
        roomtext = niceify(room)[:6]
        if room in windowsRooms and len(roomtext) == 6: # Dont truncate star
            roomtext = roomtext[:5] + "*"
        print(roomtext + ':\t', end='')
        for i in range(4):
            if roomDict[room][i] == False:
                print(TAKEN, end='') # Set red colour
            else:
                print(FREE, end='') # Set green colour
            print(indexToTime[i] + ' ', end='')
            print(ENDCOL, end='') # Reset to normal colour
        print()

def outputHTML(mydate, rooms, roomDict):
    html = "<!DOCTYPE html>\n"
    html += "<html>\n"
    html += "<head>\n"
    html += "<meta charset=\"utf-8\" />\n"
    html += "<link rel=\"stylesheet\" href=\"css.css\">\n"
    html += "<title>Lediga Datorsalar</title>\n"
    html += "</head>\n"
    html += "<body>\n"
    html += "<h1>Lediga Datorsalar " + str(mydate) + "</h1>\n"

    now = datetime.datetime.now()
    html += "<p>Senast uppdaterad: " + str(now)[:19] + "</p>\n"

    html += "<table>\n"
    for room in rooms:
        if room in windowsRooms:
            title = "title=\"Windows\""
        else:
            title = "title=\"Unix\""
        html += "<tr>\n\t<td " + title + ">" + niceify(room) + "</td>\n"
        for i in range(4):
            if roomDict[room][i] == False:
                html += "\t<td class=\"booked\" title=\"Bokad\">"
            else:
                html += "\t<td class=\"free\" title=\"Ledig\">"
            html += indexToTime[i] + "</td>\n"
        html += "</tr>\n"
    html += "</table>\n"
    html += "<footer>Maila en bättre CSS-fil till <a href=\"mailto:oi0x1cu2etw2@opayq.com\">oi0x1cu2etw2@opayq.com</a> | <a href=\"https://gitlab.com/dat14jgu/lediga-datorsalar\">lediga-datorsalar finns på gitlab</a></footer>\n"
    html += "</body>\n"
    html += "</html>\n"
    return html

def main(date, output="term"):
    url = 'https://cloud.timeedit.net/lu/web/lth1/ri1rX0x6ncw0v4QQZ1Zb1Y0Y57y1bd005X0cQq845v551w1YY7476X2647155100XY140761X07065011XY81791Y1045Y4X6615007457504YY61X3145X07661015YX64515405614X0Y150776YX105Y0X1511741565X71075414Y137556Y51Y7652X16X50071941Y7134X6604QY7X05410Q54n16507YZ104107X5867yYb9X.csv'
    (rooms, roomDict) = updateSchedule(url, mydate)
    output = output.lower()
    if output == "term":
        outputTerminal(date, rooms, roomDict)
    elif output == "html":
        print(outputHTML(date, rooms, roomDict))
    else:
        print("Illegal output specifier")
        exit(1)


parser = argparse.ArgumentParser(
    description="Fetches information about availability of computer lab rooms in the E building at LTH."
)
parser.add_argument('date', nargs="?", help="The date to generate bookings for. Must be on the format yyyy-mm-dd. If absent current date is assumed.")
parser.add_argument('-o', '--output', help="Output type. Legal values are \"html\" and \"term\"")
args = parser.parse_args()

if args.date != None:
    time = args.date.split('-')
    mydate = datetime.date(int(time[0]),int(time[1]), int(time[2]))
else:
    mydate = datetime.date.today()
if args.output != None:
    main(mydate, args.output)
else:
    main(mydate)
